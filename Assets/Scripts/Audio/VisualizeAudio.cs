﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualizeAudio : MonoBehaviour {
	public bool EnableDebug;
	public float compressorVal = 0.5f;
	public float spawnRate = 4;
	public GameObject spawnObject;
	public RectTransform button;


	string mic;
	public AudioSource src;
	int audioLength;
	State state;
	float timePerInst;
	AudioSnippet inst;
	
	enum State {
		Idle,
		Recording,
		Done
	}
	void Start(){
		mic = Microphone.devices[0];
		src = GetComponent<AudioSource>();
	}
	public void Stop(){
		state = State.Done;
	}
	void Update () {
		timePerInst = 1/spawnRate;

		switch (state){
			case State.Idle:
				Idle();
				break;
			case State.Recording:
				Recording();
				break;
		}
	}
	void Idle(){
		if(Microphone.IsRecording(mic)){
			state = State.Recording;
		}
	}

	void Recording(){
		if(!Microphone.IsRecording(mic)){
			state = State.Done;
			return;
		}
		int newAudioLength = Microphone.GetPosition(mic);
		int newSamples = newAudioLength - audioLength;
		if (newSamples <= 0) {
			return;
		}

		float[] samples = new float[newSamples * src.clip.channels];
		src.clip.GetData(samples,audioLength);

		float level = AnalyseSamples(samples);
		if(EnableDebug)
			Debug.DrawLine(new Vector3(audioLength/44100f,0,0), new Vector3(audioLength/44100f, level, 0), Color.red,999,false);

		if(inst == null || Time.time - inst.instantiated > timePerInst){
			Spawn();
		}
		if(level > inst.level){
			inst.level = level;
		}
		inst.transform.position = SpawnLoc();

		audioLength = newAudioLength;
	}
	void Spawn(){
		GameObject freshSpawn = Instantiate(spawnObject, SpawnLoc() , Quaternion.identity);
		freshSpawn.transform.parent = transform;
		inst = freshSpawn.GetComponent<AudioSnippet>();
	}
	Vector3 SpawnLoc(){
		Canvas cnvs = GetTopmostCanvas(button);
		Vector2 pos = cnvs.worldCamera.WorldToScreenPoint(button.position);
		return Camera.main.ScreenPointToRay(pos).GetPoint(0.05f);
	}
	float AnalyseSamples(float[] samples){
		float levelMax = 0;
		for (int i = 0; i < samples.Length; i++) {
			float wavePeak = samples[i];
			if (levelMax < wavePeak) {
				levelMax = wavePeak;
			}
		}
		return Mathf.Pow(levelMax,compressorVal);
	}
	public static Canvas GetTopmostCanvas(Component component) {
		Canvas[] parentCanvases = component.GetComponentsInParent<Canvas>();
		if (parentCanvases != null && parentCanvases.Length > 0) {
			return parentCanvases[parentCanvases.Length - 1];
		}
		return null;
	}
}