﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSnippet : MonoBehaviour {
	public float level;
	public float instantiated;
	float min = 0.05f;
	float radius = 0.25f;
	float rotationSpeed = 0.5f;
	float force = 150f;
	void Awake(){
		instantiated = Time.time;
	}
	void FixedUpdate(){
		float size = level * 0.5f + min;
		transform.localScale = new Vector3(min,size,min);

		Rigidbody bdy = GetComponent<Rigidbody>();
		if(bdy == null){
			return;
		}
		float targetTime = Time.time * rotationSpeed;
		targetTime += Mathf.PI * 2 * transform.GetSiblingIndex() / transform.parent.childCount;

		float x = Mathf.Cos(targetTime)*radius;
		float y = Mathf.Sin(targetTime)*radius;
		Vector3 target = new Vector3(x,0,y);
		target = transform.parent.TransformPoint(target);
		// target += transform.parent.position;
		float dist = (target - transform.parent.position).magnitude;
		dist = Mathf.Pow(dist,1.5f);
		bdy.AddExplosionForce(-1 * force * dist,target,float.PositiveInfinity);
		#if UNITY_EDITOR
		Debug.DrawLine(target,target + (Vector3.up * 0.1f),Color.red, 2);
		#endif
	}
}
