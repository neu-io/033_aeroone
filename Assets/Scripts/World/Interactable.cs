﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour {
	
	AudioSource snd;
	Animation anim;
	public iOSHapticFeedback.iOSFeedbackType fbk = iOSHapticFeedback.iOSFeedbackType.ImpactLight;
	public Material mat;
	[Range(0, 2)]
	public float pitchOffset;
	float pitchOrig;
	float volOrig;
	Material dflt;
	Rigidbody bdy;

	void Start () {
		snd = GetComponent<AudioSource>();
		pitchOrig = snd.pitch;
		volOrig = snd.volume;
		anim = GetComponent<Animation>();
		bdy = GetComponent<Rigidbody>();
		if(bdy == null){
			bdy = gameObject.AddComponent<Rigidbody>();
		}
		bdy.useGravity = false;
		bdy.constraints = RigidbodyConstraints.FreezeAll;

		if(mat != null){
			dflt = GetComponent<Renderer>().material;
		}
	}

	public virtual void Trigger(float val, Interactable orig){
		
		if(snd != null){
			snd.volume = volOrig*(val*0.15f + 0.85f);
			snd.pitch = pitchOrig + Random.Range(-pitchOffset,pitchOffset);
			snd.Play();
		}
		iOSHapticFeedback.Instance.Trigger(fbk);
		if(anim != null){
			anim.Play();
		}
		if(mat != null){
			GetComponent<Renderer>().material = mat;
		}
		AeroAnalytics.itemsHit++;
	}
	public virtual void Trigger(float val){
		Trigger(val,null);
	}
	public virtual void Trigger(){
		Trigger(1);
	}
	public virtual void Reset(){
		if(mat != null){
			GetComponent<Renderer>().material = dflt;
		}
	}
}
