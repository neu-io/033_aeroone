﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractableCollider : MonoBehaviour {
	float val;
	public enum MeasureMethod {
		None,
		Acceleration,
		Speed
	}
	public MeasureMethod msrMethod;
	public Text displayVal;
	public bool KeepEnabled = false;

	void FixedUpdate()
	{	
		if(msrMethod == MeasureMethod.Acceleration){
			CalcAccel();
		}
		if(msrMethod == MeasureMethod.Speed){
			CalcSpeed();
		}
		if(displayVal != null){
			displayVal.text = val+"";
		}
	}
	void OnCollisionEnter(Collision other){
		Interactable intr = other.gameObject.GetComponent<Interactable>();
		if(!enabled){
			return;
		}
		if(intr == null){
			return;
		}
		if(!KeepEnabled){
			enabled = false;
		}
		if(msrMethod != MeasureMethod.None){
			intr.Trigger(val);
		}else{
			intr.Trigger();
		}
	}
	void OnCollisionExit(Collision other){
		Interactable intr = other.gameObject.GetComponent<Interactable>();
		if(intr == null){
			return;
		}
		intr.Reset();
	}
	Vector3 oldpos;
	void CalcAccel(){
		Vector3 newpos = Camera.main.transform.position;
		float newval = (newpos - oldpos).magnitude;
		val = Mathf.Lerp(val,newval,0.5f);
		oldpos = newpos;
	}
	void CalcSpeed(){
		Vector3 newpos = transform.position;
		val = (newpos - oldpos).magnitude;
		oldpos = newpos;
	}
}