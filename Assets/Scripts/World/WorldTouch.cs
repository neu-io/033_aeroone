﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;



public class WorldTouch : MonoBehaviour {
	public GameObject colliderTemplate;
	GameObject[] colliders = new GameObject[11];
	void Update () {
		List<Touch> touches = InputHelper.GetTouches();
		for (int i = 0; i < touches.Count; ++i){
			
			Touch tch = touches[i];
			int id = tch.fingerId;
			
			if (isOverGUI(tch.position)){
				continue;
			}
			if(tch.phase == TouchPhase.Ended && colliders[id] != null){
				Ended(id);
			}

			int msk = 1 << 8;
			Ray ray = Camera.main.ScreenPointToRay(tch.position);
			RaycastHit hit;
			if (!Physics.Raycast (ray, out hit, 100.0F, msk)) {
				continue;
			}

            if (tch.phase == TouchPhase.Began && colliders[id] == null){
				colliders[id] = Instantiate(colliderTemplate, hit.point, Quaternion.identity);
				Draggable drg = hit.transform.gameObject.GetComponent<Draggable>();
				if(drg != null){
					drg.Connect(i);
				}
			}
        }
	}
	void Ended(int id){
		colliders[id].GetComponent<InteractableCollider>().enabled = false;
		colliders[id].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
		colliders[id] = null;
	}
	bool isOverGUI(Vector2 pos){
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
		eventDataCurrentPosition.position = pos;
		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
		return results.Count > 0;
	}
}
