﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Draggable : MonoBehaviour {
	int id = -1;
	int fingerId;
	GameObject manager;
	GameObject offset;
	GameObject placeholder;
	Quaternion startDir;

	Draggable parent;
	float start;
	void Start(){
		parent = GetComponentsInParent<Draggable>().Last();
		if(parent == this){
			parent = null;
		}
	}
	public void Connect(int id){
		if(parent != null){
			parent.Connect(id);
			return;
		}
		if(this.id >= 0){
			return;
		}
		this.id = id;
		Touch tch = InputHelper.GetTouches()[id];
		fingerId = tch.fingerId;

		manager = new GameObject();
		manager.transform.parent = Camera.main.transform;
		manager.transform.localPosition = Vector3.zero;
		manager.transform.localRotation = Quaternion.identity;

		offset = new GameObject();
		offset.transform.parent = manager.transform;
		offset.transform.localPosition = Vector3.zero;
		offset.transform.localRotation = GetCameraOffset(tch.position);

		placeholder = new GameObject();
		placeholder.transform.parent = gameObject.transform.parent;
		placeholder.transform.SetSiblingIndex(gameObject.transform.GetSiblingIndex());
		gameObject.transform.parent = offset.transform;

		start = Time.time;

		dragList.Add(this);
	}
	public void Disconnect(){
		if(parent != null){
			parent.Disconnect();
			return;
		}
		gameObject.transform.parent = placeholder.transform.parent;
		gameObject.transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());

		
		Destroy(manager);
		Destroy(offset);
		Destroy(placeholder);

		dragList.Remove(this);

		id = -1;
	}
	public bool dragged{
		get{
			return id == -1;
		}
	}
	void Update(){
		if(id < 0){
			return;
		}
		List<Touch> tchList = InputHelper.GetTouches();
		if(tchList.Count <= id){
			Disconnect();
			return;
		}
		Touch tch = tchList[id];
		if(tch.phase == TouchPhase.Ended || tch.phase == TouchPhase.Canceled || tch.fingerId != fingerId){
			Disconnect();
			GameObject trsh = TrashButton.IsOver(tch.position);
			if(trsh != null){
				trsh.GetComponent<TrashButton>().Trash(gameObject);
			}
			return;
		}
		if(tch.phase == TouchPhase.Moved || tch.phase == TouchPhase.Stationary){
			offset.transform.localRotation = GetCameraOffset(tch.position);
			MuteAudio();	
		}
		
	}
	void MuteAudio(){
		float duration = Time.time - start;
		if(duration < 0.2f){
			return;
		}
		AudioSource[] srcList = GetComponentsInChildren<AudioSource>();
		foreach(AudioSource src in srcList){
			src.volume = 1 - (duration - 0.2f) * 5;
		}
	}
	Quaternion GetCameraOffset(Vector2 pos){
		Camera cam = Camera.main;

		Vector3 dir = cam.ScreenPointToRay(pos).direction;
		dir = cam.transform.InverseTransformDirection(dir);
		Vector3 forward = cam.transform.InverseTransformDirection(cam.transform.forward);

		return Quaternion.FromToRotation(forward,dir);
	}
	static List<Draggable> dragList = new List<Draggable>();
	public static bool isDragging(){
		return dragList.Count > 0;
	}
}
