﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationDebug : MonoBehaviour {
	public DeviceOrientation overrideOrient;
	public static DeviceOrientation overrideOrientation;
	void Update(){
		overrideOrientation = overrideOrient;
	}
	public static DeviceOrientation Orientation(){
		if(overrideOrientation != DeviceOrientation.Unknown){
			return overrideOrientation;
		}
		return Input.deviceOrientation;
	}
}
