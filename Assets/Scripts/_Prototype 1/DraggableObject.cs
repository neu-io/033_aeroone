﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]

public class DraggableObject : MonoBehaviour {

	private bool mousePressed = false;
	private bool selected = false;
	private string state = "";
	public float minTimeMousePressedToStartDrag = 0.02f;
	private float distance;
	private Collider coll;
	private Vector3 dragOffset = Vector3.zero;
	// Use this for initialization
	void Start () {
		coll = this.GetComponent<Collider> ();
	}
	
	// Update is called once per frame
	void Update () {
		float timeMouseDown = 0;

		if (Input.GetMouseButtonDown (0)) {
			Debug.Log ("MouseButton - - - -DOWN");
			
			mousePressed = true;
			timeMouseDown = Time.time;

			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (coll.Raycast (ray, out hit, 100.0F)) {
				selected = true;
				Debug.Log ("Selected");
				distance = hit.distance;
				dragOffset = hit.point - transform.position;
			}
			
		}

		// DRAGGING UPDATES
		if (mousePressed) { 
			if (state != "DRAGGING") {	
				if (selected && Time.time - timeMouseDown > minTimeMousePressedToStartDrag) {
					Debug.Log ("STATE CHANGE: from " + state + " to DRAGGING");
					state = "DRAGGING";
				}
			} else {
				// DRAGGING OBJECT AROUND
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				Vector3 pMouse = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				transform.position = ray.GetPoint (distance) - dragOffset;
			}
		}

		if (Input.GetMouseButtonUp (0)) {
			Debug.Log ("MouseButton - - - UP + time: " + (Time.time - timeMouseDown));
			//Debug.Log ("selectedAudioTag: " + selectedAudioTag);
			if (state == "DRAGGING") {

				// RELEASE DRAGGED OBJECT
				Debug.Log ("STATE CHANGE: from " + state + " to NONE ");
				state = "";

				//if (Input.mousePosition.y < 100) {
					//DestroyAudio (selectedAudioTag);
					//Destroy(this.gameObject);

				//}

			}

			timeMouseDown = 0;
			mousePressed = false;
			selected = false;
		}
	}
}
