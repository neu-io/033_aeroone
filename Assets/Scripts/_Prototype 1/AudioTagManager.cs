﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTagManager : MonoBehaviour {

	public GameObject prefab;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		

	}

	public void CreateNewAudioTag(Vector3 pos)
	{
		Debug.Log ("CreateNewAudioTag: " + pos);
		GameObject audioTag = GameObject.Instantiate(prefab , pos, new Quaternion() );
		audioTag.GetComponent<ScaleUpScaleDown> ().Normal ();
		audioTag.transform.parent = transform;
	}


}
