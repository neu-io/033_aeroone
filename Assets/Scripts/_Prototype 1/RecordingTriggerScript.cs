﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RecordingTriggerScript : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	public AudioManager audioManager;


	public void OnPointerDown(PointerEventData data)
	{

		audioManager.StartRecording ();
	}


	public void OnPointerUp(PointerEventData data)
	{

		audioManager.StopRecording ();
	}

	void Start () {
	}

	void Update () 
	{
	}
}
