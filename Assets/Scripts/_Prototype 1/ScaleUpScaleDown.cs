﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleUpScaleDown : MonoBehaviour {


	public float initialScale = .01f;
	public float highlightScale = 1.2f;
	public float normalScale = 0.8f;
	private float targetScale = 1.0f;

	private bool scaling = false;

	// Use this for initialization
	void Awake () {
		targetScale = initialScale;
		this.transform.localScale = new Vector3 (initialScale, initialScale, initialScale);
	}
	
	// Update is called once per frame
	void Update () {
		
		float scale = this.transform.localScale.x;
	
		if (scaling) {
			scale += (targetScale - scale) / 8.0f;
			this.transform.localScale = new Vector3 (scale, scale, scale);
		}


		if (Mathf.Abs(targetScale - scale ) < .0001f) 
		{
			scaling = false;
			scale = targetScale;
			this.transform.localScale = new Vector3 (scale, scale, scale);
		}


	}

	public void Highlight () {
		scaling = true;
		targetScale = highlightScale;
	}

	public void Normal () {
//		Debug.Log( "Normal");
		scaling = true;
		targetScale = normalScale;
//		Debug.Log( "targetScale: " + targetScale);
	}

	public void Initial () {
//		Debug.Log( "Initial");
		scaling = true;
		targetScale = initialScale;
	}


	public void SetScaling (bool b) {
//		Debug.Log ("SetScaling " + b);
		scaling = b;
	}



}
