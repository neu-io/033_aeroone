﻿	using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour {
	

	private GameObject cameraGameObject;
	public Button buttonRecord;
	public Button button3D;
	//public Image CrosshairBG;
	public Image TrashCanImage;
	public Transform map;
	public Transform prefab;

	public float maxDistance = 1.0f;
	private AudioRecorder audioRec;

	// states //
	// NONE, RECORDING, DRAGGING, PLAYING

	public string state = "NONE";
	private bool mapMode = false;
	private bool threeDMode = false;

	private float initialRecordButtonScale = 1.0f;
	private Vector3 initialRecordButtonPosition;
	private float draggingDistance = 0.0f;
	private Vector3 draggingOffset = Vector3.zero;
	private bool mousePressed = false;
	private float timeMouseDown = 0;

	public float minTimeMousePressedToStartDrag = .3f;
	private int audioClipID = 0;

	public float initialAudioRecordingDistance = .3f;

	public string audioClipName = "myAudioClip";

	public Color defaultColor;
	public Color movingColor;
	 
	private Vector3 camPos;
	public float mapDistance = 1.0f;


	//public Slider slider;

	public GameObject selectedAudioTag;

	int layerMask;

	private bool proximityMode = false;

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	void Awake () {
		layerMask = LayerMask.GetMask("AudioTags") ;
		cameraGameObject = Camera.main.gameObject;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	void Start () {
		Application.RequestUserAuthorization(UserAuthorization.Microphone);
		//slider.onValueChanged.AddListener(SliderListener);

		initialRecordButtonScale = buttonRecord.transform.localScale.x;
		initialRecordButtonPosition = buttonRecord.transform.position;


	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	void Update () {

		if (state != "RECORDING") {
			
			if (Input.GetMouseButtonDown (0)) {
				Debug.Log ("MouseButton - - - -DOWN");
				if (state != "PLAYING") {
					mousePressed = true;
					timeMouseDown = Time.time;

					Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
					RaycastHit hit;
					if (Physics.Raycast (ray, out hit, 100.0F, layerMask)) {
				
						Debug.DrawLine (ray.origin, hit.point);
						selectedAudioTag = hit.transform.gameObject;
						draggingDistance = hit.distance;
						draggingOffset = hit.point - hit.transform.position;
						Debug.Log ("Selected Audio Tag:" + selectedAudioTag.name);
					}
				}
			}

			// DRAGGING UPDATES
			if (mousePressed) { 
				if (state != "DRAGGING" ) {	
//					Debug.Log ("mouse pressed for " + (Time.time - timeMouseDown) + " seconds");
					//Debug.Log ();
					if (selectedAudioTag != null) {
						if (Time.time - timeMouseDown > minTimeMousePressedToStartDrag) {
							Debug.Log ("STATE CHANGE: from " + state + " to DRAGGING");
							state = "DRAGGING";

//							Handheld.Vibrate();
						}
					}
				} else {
					// DRAGGING OBJECT AROUND
					TrashCanImage.enabled = true;
					Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
					buttonRecord.transform.localScale = new Vector3 (initialRecordButtonScale, initialRecordButtonScale, initialRecordButtonScale) * 7.0f;
					buttonRecord.transform.position = new Vector3(initialRecordButtonPosition.x, initialRecordButtonPosition.y - 400, initialRecordButtonPosition.z);

					//Vector3 pMouse = Camera.main.ScreenToWorldPoint (Input.mousePosition);

					selectedAudioTag.transform.position = ray.GetPoint (draggingDistance) - draggingOffset;

					//selectedAudioTag.transform.position = cameraGameObject.transform.position + cameraGameObject.transform.forward * initialAudioRecordingDistance;
				}
			}
	
			if (Input.GetMouseButtonUp (0)) {
				Debug.Log ("MouseButton - - - UP + time: " + (Time.time - timeMouseDown));
				//Debug.Log ("selectedAudioTag: " + selectedAudioTag);
				if (state == "DRAGGING") {

					// RELEASE DRAGGED OBJECT
					Debug.Log ("STATE CHANGE: from " + state + " to NONE ");
					state = "NONE";

					if (Input.mousePosition.y < 280) {
						DestroyAudio (selectedAudioTag);
					}
					selectedAudioTag = null;


				} else {
					if (state != "PLAYING" && selectedAudioTag != null) {
						if (Time.time - timeMouseDown < minTimeMousePressedToStartDrag) {
							//TODO: add maximum distance limit
							PlayAudio (selectedAudioTag);
							Debug.Log ("STATE CHANGE: from " + state + " to PLAYING");
							state = "PLAYING";

						}
					}
				}

				timeMouseDown = 0;
				mousePressed = false;
				TrashCanImage.enabled = false;
				buttonRecord.transform.localScale = new Vector3 (initialRecordButtonScale, initialRecordButtonScale, initialRecordButtonScale);
				buttonRecord.transform.position = new Vector3(initialRecordButtonPosition.x, initialRecordButtonPosition.y, initialRecordButtonPosition.z);
			}

			if (state == "PLAYING") {

//				float pct = selectedAudioTag.GetComponent<AudioRecorder> ().pctcmplt;
//				selectedAudioTag.transform.GetChild (1).GetComponent<TextMesh> ().text = pct.ToString ();
				//Debug.Log ( "PLAYING : " + pct);

				if (selectedAudioTag.GetComponent<AudioRecorder> ().done) {
					Debug.Log ("STATE CHANGE: from " + state + " to NONE");
					state = "NONE";
					StopAudio (selectedAudioTag);
					selectedAudioTag = null;
				}
			}
		}
	}

	// - - - - - - - - - - - - - - - UI - - - - - - - - - - - - -


	public void SliderListener(float value)
	{
		Debug.Log ("SliderListener: change maxDistance");
		foreach (Transform child in transform) {

			AudioSource audioSource = child.GetComponent<AudioSource> ();
			audioSource.maxDistance = value;
			maxDistance = value;
		}
	}


	// - - - - - - - - - - - - - - - RECORDING - - - - - - - - - - - - -


	public void StopRecording ()
	{
		Debug.Log ("Stop Recording");
		Debug.Log ("STATE CHANGE: from " + state + " to NONE");
		state = "NONE";
		if (audioRec != null) {
			//done with recording
			//scale up
			audioRec.GetComponent<ScaleUpScaleDown> ().Normal();
			audioRec.StopRecord ();
			audioRec.SaveToDisk (audioClipName + audioClipID.ToString ());
			AudioSource audioSource = audioRec.GetComponent<AudioSource> ();
			audioSource.maxDistance = maxDistance;

			if (threeDMode) {
				PlayAndLoop (audioRec.gameObject);

			}
		}
		audioClipID++;
	
		//buttonRecordBG.color = Color.white;
	}
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	//records audio and saves it "to the disk"
	public void StartRecording ()
	{
		Debug.Log ("Start Recording");
		Debug.Log ("STATE CHANGE: from " + state + " to RECORDING");
		state = "RECORDING";
	

		string title = audioClipName + audioClipID.ToString();

		//contains AudioRecorder & AudioSource
		Vector3 pos = cameraGameObject.transform.position + cameraGameObject.transform.forward * initialAudioRecordingDistance;;
		Transform audioTag = GameObject.Instantiate(prefab,pos, new Quaternion() );

		audioTag.parent = this.transform;

		audioTag.GetComponent<TriggerSoundOnApproach> ().enabled = proximityMode;

		if (mapMode) {

			Ray ray = Camera.main.ScreenPointToRay (new Vector3 (375, 667, 0));
			RaycastHit hit;
			if (map.GetComponent<Collider>().Raycast (ray, out hit, 100.0F)) {
				audioTag.position = hit.point;
			}
		} else {

		}


		// replacing current recording with new component
		audioRec = audioTag.GetComponent<AudioRecorder>();
		audioRec.id = audioClipID;
		audioRec.title = title;
		audioRec.Record ();
		audioRec.myColor = Color.HSVToRGB (Random.Range(.55f, .70f), 1.0f, 1.0f);


		// sphere coloring
		//audioTag.GetComponent<Renderer> ().material.color = audioRec.myColor;
		audioTag.GetComponent<Renderer> ().material.SetFloat("_PhaseOffset", Random.value);
		//Debug.Log ("audio clip nr: " + audioClipID);

	}


	// - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	public void ToggleProximityMode ()
	{
		proximityMode = !proximityMode;
		foreach (Transform child in transform) {
			TriggerSoundOnApproach trigger = child.GetComponent<TriggerSoundOnApproach> ();
			if (trigger != null) {
				
				trigger.enabled = proximityMode;
			}

		}
		//TODO change button Color

	}
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	public void Toggle3D ()
	{
		threeDMode = !threeDMode;
	
		if (threeDMode) {
			button3D.GetComponent<Image> ().color = Color.red;
			PlayAllAudio ();

		} else {
			button3D.GetComponent<Image> ().color = Color.white;
			StopAllAudio ();
		}
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	public void StopAllAudio( )
	{

		foreach (Transform child in transform) {
			AudioSource audioSource = child.GetComponent<AudioSource> ();
			audioSource.loop = false;

			AudioRecorder audioRec = child.GetComponent<AudioRecorder> ();
			audioRec.StopPlay ();
		}
		Debug.Log ("Stopped all audio");
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	public void PlayAllAudio( )
	{

		foreach (Transform child in transform) {
			PlayAndLoop (child.gameObject);
		}
		Debug.Log ("Playing all audio");
	}

	public void PlayAndLoop (GameObject audioTag){
		AudioSource audioSource = audioTag.GetComponent<AudioSource> ();
		audioSource.loop = true;

		AudioRecorder audioRec = audioTag.GetComponent<AudioRecorder> ();
		audioRec.Play ();
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		
	public void PlayAudio( GameObject audioTag)
	{
		
		HighlightAudioRecording (audioTag);
		AudioRecorder audioRec = audioTag.GetComponent<AudioRecorder> ();
		audioRec.Play ();

		Debug.Log ("PlayAudio");
	}


	// - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	public void StopAudio( GameObject audioTag)
	{
		UnlightAudioRecording (audioTag);
		AudioRecorder audioRec = audioTag.GetComponent<AudioRecorder> ();
		audioRec.StopPlay ();

		Debug.Log ("StopAudio");
	}


	// - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	public void HighlightAudioRecording( GameObject audioTag)
	{
		
		audioTag.GetComponent<Renderer>().material.color = Color.red;
		ScaleSprite (audioTag, true);

		Debug.Log ("Highlight Audio");
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	public void UnlightAudioRecording( GameObject audioTag)
	{
		
		audioTag.GetComponent<Renderer>().material.color = audioTag.GetComponent<AudioRecorder>().myColor;
		ScaleSprite (audioTag, false);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	public void ScaleSprite(GameObject audioTag, bool up){
		if (up) {
			audioTag.GetComponent<ScaleUpScaleDown> ().Highlight ();
		} else {
			audioTag.GetComponent<ScaleUpScaleDown> ().Normal ();
		}
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	public void StartMovingAudio ()
	{
		Debug.Log ("Start moving Audio");

		//currentChild.GetChild(1).GetComponent<SpriteRenderer> ().color = movingColor;
	}


	// - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	public void StopMovingAudio ()
	{
		Debug.Log ("Stop moving Audio");


		//currentChild.GetChild(1).GetComponent<SpriteRenderer> ().color = defaultColor;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	public void DestroyAudio (GameObject GO)
	{
		Debug.Log ("Destroy Audio");

		StopAudio (GO);
		Destroy (GO);

	}
}
