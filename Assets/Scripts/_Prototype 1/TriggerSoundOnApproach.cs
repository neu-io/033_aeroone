﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Collider))]
[RequireComponent(typeof(AudioSource))]

public class TriggerSoundOnApproach : MonoBehaviour {

	//private Collider m_collider;
	// Use this for initialization
	void Start () {
		this.GetComponent<Collider> ().isTrigger = true;
	}
	
	// Update is called once per frame
	void Update () {
		//m_collider.isTrigger
		//if (Camera.main
	}

	void OnTriggerEnter(Collider other){
		if (this.enabled)
		{
		Debug.Log ("Collision");
		if (other.gameObject == Camera.main.gameObject)
		this.GetComponent<AudioSource> ().Play ();
//		Handheld.Vibrate();
		}
	}
}
