﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class AeroAnalytics : MonoBehaviour {
	public static int itemsRecorded;
	public static int itemsSpawned;
	public static int itemsHit;
	public static int itemsDeleted;

	public static float timeRecorded;
	void OnApplicationPause(bool pauseStatus){
		if(!pauseStatus){
			return;
		}
		Analytics.CustomEvent("Stats", new Dictionary<string, object>
        {
            { "recordings", itemsRecorded },
            { "spawned", itemsSpawned },
			{ "hit", itemsHit },
			{ "deleted", itemsDeleted },
			{ "timeRecorded", timeRecorded }
        });

		itemsRecorded = 0;
		itemsSpawned = 0;
		itemsHit = 0;
		itemsDeleted = 0;
		timeRecorded = 0;
	}
	public static void CompletedTutorial(){
		PlayerPrefs.SetInt("CompletedTutorial",1);
		Analytics.CustomEvent("CompletedTutorial");
	}
}
