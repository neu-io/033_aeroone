﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallToCenter : MonoBehaviour {
	void FixedUpdate(){
		Rigidbody bdy = GetComponent<Rigidbody>();
		if(bdy == null){
			return;
		}
		bdy.AddExplosionForce(-1.1f,new Vector3(0,0,1),float.PositiveInfinity);
	}
}
