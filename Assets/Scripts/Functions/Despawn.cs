﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Despawn : MonoBehaviour {
	public float timeToDespawn = 5;
	float spawnTime;
	void Start () {
		spawnTime = Time.time;
	}
	void Update () {
		if(Time.time - spawnTime > timeToDespawn){
			Destroy(gameObject);
		}
	}
}
