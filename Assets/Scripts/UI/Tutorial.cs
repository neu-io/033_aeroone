﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class Tutorial : MonoBehaviour {
	public string name;
	void Start(){
		tList.Add(this);
		UpdateState();
	}
	void UpdateState(){
		gameObject.SetActive(!PlayerPrefs.HasKey(name));
	}
	public void Complete(){
		PlayerPrefs.SetInt(name,1);
		PlayerPrefs.Save();
		gameObject.SetActive(false);

		if(PlayerPrefs.HasKey("CompletedTutorial")){
			return;
		}
		bool completed = true;;
		foreach(Tutorial tInst in tList){
			if(!PlayerPrefs.HasKey(tInst.name)){
				completed = false;
			}
		}
		if(!completed){
			return;
		}
		AeroAnalytics.CompletedTutorial();
	}
	public virtual void Trigger(){
		Complete();
	}
	public virtual void Reset(){

	}

	public void ResetAll(){ ResetTutorial(); }
	static List<Tutorial> tList = new List<Tutorial>();
	static public void ResetTutorial(){
		foreach(Tutorial tInst in tList){
			PlayerPrefs.DeleteKey(tInst.name);
			tInst.UpdateState();
		}
		PlayerPrefs.Save();
	}
	static public void CompleteItem(string item){
		foreach(Tutorial tInst in tList){
			if(tInst.name == item){
				tInst.Complete();
			}
		}
	}
}
