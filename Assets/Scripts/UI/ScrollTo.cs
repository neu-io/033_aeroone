﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScrollTo : MonoBehaviour, IBeginDragHandler {
	enum State {
		Idle,
		Scrolling
	}
	State state = State.Idle;
	public float lerpSpeed = 0.2f;
	float scrollTarget;
	void LateUpdate () {
		if(state == State.Scrolling){
			ScrollRect rct = GetComponent<ScrollRect>();
			float pos = rct.horizontalNormalizedPosition;
			pos = Mathf.Lerp(pos,scrollTarget,lerpSpeed);
			if(Mathf.Abs(pos - scrollTarget) < 0.002f){
				state = State.Idle;
				pos = scrollTarget;
			}
			rct.horizontalNormalizedPosition = pos;
		}
	}
	public void SetTarget(float trgt){
		state = State.Scrolling;
		scrollTarget = trgt;
	}
	public void OnBeginDrag(PointerEventData eventData ){
		state = State.Idle;
	}
}