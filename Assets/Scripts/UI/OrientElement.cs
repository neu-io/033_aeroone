﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientElement : MonoBehaviour {
	public bool step;
	public float limit;
	static float animDuration = 0.35f;
	float targetOrientation = 0;
	public float orientation = 0;
	void Update () {
		if(step){
			switch (OrientationDebug.Orientation()){
				case DeviceOrientation.LandscapeLeft:
					targetOrientation = -90;
					break;
				case DeviceOrientation.LandscapeRight:
					targetOrientation = 90;
					break;
				case DeviceOrientation.PortraitUpsideDown:
					targetOrientation = 180;
					break;
				default:
					targetOrientation = 0;
					break;
			}
			float diff = targetOrientation - orientation;
			float offset = Time.deltaTime / animDuration * 90;
			if(Mathf.Abs(diff) < offset){
				orientation = targetOrientation;
			}else{
				orientation += offset*Mathf.Sign(diff);
			}
		}else{
			orientation = -Camera.main.transform.eulerAngles.z;
			if(orientation<-180){
				orientation+=360;
			}
		}
		if(limit != 0){
			orientation = Mathf.Clamp(orientation,-limit,limit);
		}

		Transform trns = GetComponent<Transform>();
		if(trns != null){
            Vector3 rot = trns.eulerAngles;
            rot.z = orientation;
			trns.rotation = Quaternion.Euler(rot);
		}
		RectTransform rtrns = GetComponent<RectTransform>();
		if(rtrns != null){
			Vector3 rot = trns.eulerAngles;
            rot.z = orientation;
			trns.rotation = Quaternion.Euler(rot);
		}
	}
}
