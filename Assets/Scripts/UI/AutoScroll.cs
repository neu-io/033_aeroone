﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AutoScroll : MonoBehaviour, IEndDragHandler {
	ScrollRect scrl;
	void Start(){
		scrl = GetComponent<ScrollRect>();
	}
	public void OnEndDrag (PointerEventData eventData) {
		if(scrl.velocity.magnitude > 10){
			return;
		}
		if(scrl.horizontalNormalizedPosition < 0.5){
			GetComponent<ScrollTo>().SetTarget(0);
		}
		if(scrl.horizontalNormalizedPosition >= 0.5){
			GetComponent<ScrollTo>().SetTarget(1);
		}
	}
}
