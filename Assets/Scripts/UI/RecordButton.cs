﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RecordButton : MonoBehaviour, IPointerDownHandler {
	AudioSource aud;
	string mic;
	public GameObject template;
	GameObject current;
	float distanceFromCamera = 0.3f;
	public iOSHapticFeedback.iOSFeedbackType fbk;
	void Start(){
		iPhoneSpeaker.ForceToSpeaker();
	}
	void Update(){
		GetComponent<Animator>().SetBool("Hide",Draggable.isDragging());
	}
	public void OnPointerDown(PointerEventData eventData)
	{
		iOSHapticFeedback.Instance.Trigger(fbk);
		GetComponent<Animator>().SetBool("Down",true);
		iPhoneSpeaker.ForceToSpeaker();

		mic = Microphone.devices[0];

		current = Instantiate(template, Camera.main.transform.position + (this.distanceFromCamera * Camera.main.transform.forward), Quaternion.identity);
		current.GetComponent<VisualizeAudio>().button = GetComponent<RectTransform>();
		current.transform.SetParent(Camera.main.transform);
		AeroAnalytics.itemsRecorded++;

		aud = current.GetComponent<AudioSource>();
		aud.clip = Microphone.Start(mic, true, 5*60, 44100);
	}
	public void OnPointerUp(){
		GetComponent<Animator>().SetBool("Down",false);
		if(Microphone.IsRecording(mic)){
			current.GetComponent<VisualizeAudio>().Stop();
			current.transform.SetParent(null);
			StopRecord(mic);
			aud = null;
		}
	}
	void StopRecord(string mic) { 
		int lastTime = Microphone.GetPosition(null); 
		if (lastTime == 0){
			return;
		}
		AeroAnalytics.timeRecorded += lastTime/441000f;
		Microphone.End(null);

		float[] samples = new float[aud.clip.samples];
		aud.clip.GetData(samples, 0);
		float[] ClipSamples = new float[lastTime];
		System.Array.Copy(samples, ClipSamples, ClipSamples.Length - 1);

		aud.clip = AudioClip.Create("playRecordClip", ClipSamples.Length, 1, 44100, false);
		aud.clip.SetData(ClipSamples, 0);
	}
}
