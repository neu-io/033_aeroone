﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems; 
using System.Linq;

public class Menu_Item : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
	public GameObject Prefab;
	public GameObject WorldUIPrefab;
	public GameObject ThreeDObject;
	GameObject itm;
	GameObject model;
	public static float targetSize = 0.135f;
	bool isClickable = true;
	void Start(){
		// GetComponent<RectTransform>().anchoredPosition =  new Vector2(transform.GetSiblingIndex() * 135,0);
		gameObject.name = "Menu_item - "+Prefab.name;
		GameObject parent = Instantiate(Prefab);
		parent.transform.localPosition = new Vector3(0,0,0);

		itm = Instantiate(WorldUIPrefab);
        itm.name = "3D Menu_item - "+Prefab.name;

		model = new GameObject();
		model.transform.parent = itm.transform;
		OrientElement el = model.AddComponent<OrientElement>();
		el.step = true;
		foreach(Transform child in parent.GetComponentsInChildren<Transform>()){
			child.parent = model.transform;
			Destroy(child.gameObject.GetComponent<SphereCollider>());
			Destroy(child.gameObject.GetComponent<BoxCollider>());
			Destroy(child.gameObject.GetComponent<AudioSource>());
		}

		Destroy(parent);

		PlaceOnUI uiitem = itm.GetComponent<PlaceOnUI>();
		uiitem.uiElement = ThreeDObject;
		MoveToLayer(itm.transform,9);

		Renderer[] renderers = itm.GetComponentsInChildren<Renderer>();
		Bounds combinedBounds = renderers[0].bounds;
		foreach (Renderer render in renderers) {
			combinedBounds.Encapsulate(render.bounds);
		}

		Vector3 scl = combinedBounds.size;
		scl = model.transform.localScale *  targetSize/Mathf.Max(scl.x,scl.y,scl.y);
		model.transform.localScale = scl;
	}
	public void Spawn(){
		if(!isClickable){
			return;
		}
		GameObject obj = Instantiate(Prefab);
		Vector3 pos = obj.transform.position;
		obj.transform.SetParent(Camera.main.transform);
		obj.transform.localRotation = Quaternion.identity;
		Vector3 rot = obj.transform.eulerAngles;
		obj.transform.rotation = Quaternion.Euler(0,rot.y,0);
		obj.transform.localPosition = pos;
		obj.transform.SetParent(null);

		itm.GetComponent<Animator>().SetTrigger("Spawned");
		Tutorial.CompleteItem("TutDrag");
		AeroAnalytics.itemsSpawned++;

	}
	
	ScrollRect scrollViewContainer;
	Vector3 dragOffset;
	Vector3 origScale;
	public void OnBeginDrag(PointerEventData eventData ){
		isClickable = false;
		dragOffset = Vector3.zero;
		origScale = model.transform.localScale;
		scrollViewContainer = GetComponentsInParent<ScrollRect>().Last();
		if(scrollViewContainer){
			scrollViewContainer.SendMessage( "OnBeginDrag", eventData );
		}
	}
	float dragLength = 550;
	float dragGate = 125;
	public void OnDrag(PointerEventData eventData ){
		if(scrollViewContainer){
			scrollViewContainer.SendMessage( "OnDrag", eventData );
		}
		Vector3 offset = eventData.delta;
		offset.x = 0;
		dragOffset += offset;
		float y = dragOffset.y;
		if(y > dragGate){
			y = Mathf.Min(y,dragGate+dragLength*1.5f);
			y = dragGate + Mathf.Sin((y - dragGate)/dragLength * Mathf.PI / 3 )*dragLength;
		}
		y = Mathf.Max(y,0);
		RectTransform TheeDTrns = ThreeDObject.GetComponent<RectTransform>();
		TheeDTrns.localPosition = new Vector2(0,y);
		float size = Mathf.InverseLerp(dragGate/2, dragLength + dragGate,y);
		float scale = Mathf.Lerp(1,5,size);
		model.transform.localScale = Vector3.Lerp(origScale,Vector3.one*scale,size);
		itm.transform.localPosition = new Vector3(0,0,size);

	}
	public void OnEndDrag(PointerEventData eventData ){
		isClickable = true;
		model.transform.localScale = origScale;
		RectTransform TheeDTrns = ThreeDObject.GetComponent<RectTransform>();
		if(TheeDTrns.localPosition.y > dragGate){
			Spawn();
		}
		TheeDTrns.localPosition = new Vector2(0,0);
		if(scrollViewContainer){
			scrollViewContainer.SendMessage( "OnEndDrag", eventData );
		}
	}
	void MoveToLayer(Transform root, int layer) {
		root.gameObject.layer = layer;
		foreach(Transform child in root)
			MoveToLayer(child, layer);
	}
}
