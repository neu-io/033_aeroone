﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FullScreen : MonoBehaviour {
	RectTransform rectTransform;
	RectTransform canvasRectTransform;
	void Start(){
		rectTransform = GetComponent<RectTransform>();
		if (rectTransform == null){
			gameObject.SetActive(false);
		}
		if(rectTransform.root != null){
			canvasRectTransform = rectTransform.root.GetComponent<RectTransform>();
			if (canvasRectTransform == null){
				gameObject.SetActive(false);
			}
		}
	}
	void Update (){
		rectTransform.sizeDelta = canvasRectTransform.sizeDelta;
    }
}
