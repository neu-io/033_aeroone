﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceOnUI : MonoBehaviour {
	public GameObject uiElement;
	void LateUpdate(){
		if(uiElement == null){
			gameObject.SetActive(false);
			Debug.LogError("No associated ui element");
		}
		RectTransform rct = uiElement.GetComponent<RectTransform>();
		Vector3 centre = Vector3.zero;
		Vector3[] crners = new Vector3[4];
		rct.GetWorldCorners(crners);
		foreach(Vector3 crner in crners){
			centre += crner/4;
		}
		centre.z = 5;
		transform.position = centre;
	}
}
