﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialDuration : Tutorial {
	public float minDuration;
	float TriggerStart;
	public override void Trigger(){
		TriggerStart = Time.time;
	}
	public override void Reset(){
		float duration = Time.time - TriggerStart;
		if(duration > minDuration){
			this.Complete();
		}
	}
}
