﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TrashButton : MonoBehaviour {
	public GameObject recButton;
	public bool prevDrag;
	bool show;
	public float lastHeld;
	void Awake(){
		if(gr == null){
			gr = GetComponent<GraphicRaycaster>();
		}
	}
	void Update(){
		bool isDragging = Draggable.isDragging();
		if(prevDrag != isDragging){
			lastHeld = Time.time;
			prevDrag = isDragging;
		}
		show = Time.time - lastHeld > 0.25f && isDragging;
		GetComponent<Animator>().SetBool("Hide",!show);
		recButton.GetComponent<Animator>().SetBool("Hide",show);
	}
	public void Trash(GameObject obj){
		obj.AddComponent<Despawn>().timeToDespawn = 0.1f;;
		AeroAnalytics.itemsDeleted++;
	}
	void MoveToLayer(Transform root, int layer) {
		root.gameObject.layer = layer;
		foreach(Transform child in root)
			MoveToLayer(child, layer);
	}
	static GraphicRaycaster gr;
	public static GameObject IsOver(Vector2 pos){
			PointerEventData ped = new PointerEventData(null);
			ped.position = pos;
			List<RaycastResult> results = new List<RaycastResult>();
			gr.Raycast(ped, results);
			foreach(RaycastResult rslt in results){
				if(rslt.gameObject.GetComponent<TrashButton>() != null){
					return rslt.gameObject;
				}
			}
			return null;
	}

}
