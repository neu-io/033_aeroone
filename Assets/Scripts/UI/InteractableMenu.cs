﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems; 

public class InteractableMenu : MonoBehaviour {
	public List<GameObject> menuItems = new List<GameObject>();
	public GameObject MenuItemTemplate;
	public Transform holder;
	void Start(){
		foreach(GameObject itm in menuItems){
			if(itm == null){
				continue;
			}
			GameObject menuItem = Instantiate(MenuItemTemplate);
			menuItem.transform.SetParent(holder,false);
			menuItem.GetComponent<Menu_Item>().Prefab = itm;
		}
		ScrollRect rct = GetComponent<ScrollRect>();
		rct.horizontalNormalizedPosition = 1;
		GetComponent<ScrollTo>().SetTarget(0);
	}
}
