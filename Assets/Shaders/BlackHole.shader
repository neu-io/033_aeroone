﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "AERO/BlackHole"
{
    Properties
    {
    	 _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}

    	 _BlackHoleRadius ("Black Hole Radius", Range(0.0,2.0)) = 0.8
        _TwirlRadius ("Twirl Radius", Range(0.0,2.0)) = 0.8
        _TwirlAngle ("Twirl Angle", Range(0.0,1.0)) = 0.8
        _TwirlCenter ("Twirl Center", Vector) = (.5, .5, 0, 0)

      // _Fresnel ("Fresnel", Range(0.0,1)) = 0.2

       //_RefractionStrength("Refraction Strength",Range(0.0,10)) = 2.0
       	
         //_Ramp ("Shading Ramp", 2D) = "gray" {}
         //  _Attenuation ("Attenuation", Range(0.1,1)) = .4
	  	//_SpecExpon ("Spec Power", Range (0, 125)) = 12
	  	//_FilmDepth ("Film Depth", Range (0, 1)) = 0.05
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
       
        GrabPass { }

        //Cull Off
       	//Blend SrcAlpha OneMinusSrcAlpha
        //ZWrite On

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma fragmentoption ARB_precision_hint_fastest
 
            #include "UnityCG.cginc"
            #include "UnityStandardBRDF.cginc"
 
            struct appdata
            {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
                float3 normal:TEXCOORD1;
            };
 
            struct v2f
            {
                float4 pos : SV_POSITION;
                float4 texcoord : TEXCOORD0;
                float4 grabUV : TEXCOORD1;
                float3 worldPos:TEXCOORD2;
                float3 normal:TEXCOORD3;
                float3 TtoV0 : TEXCOORD4;
				float3 TtoV1 : TEXCOORD5;
                float3 viewDir:TEXCOORD6;
            
            };

           //TWIRL
         
           	float _TwirlRadius;
			float _TwirlAngle;
			float2 _TwirlCenter;
			float _BlackHoleRadius;

		
		    sampler2D _MainTex;
           	float4 _MainTex_ST;
            sampler2D _GrabTexture;
           // sampler2D _RefractionMap;
         
            float4 _GrabTexture_TexelSize;
         

            v2f vert (appdata_full v)
            {
                v2f o;

                TANGENT_SPACE_ROTATION;
                o.TtoV0 = mul(rotation, UNITY_MATRIX_IT_MV[0].xyz);
				o.TtoV1 = mul(rotation, UNITY_MATRIX_IT_MV[1].xyz);

		        o.normal = v.normal;
         		o.texcoord.xy = TRANSFORM_TEX(v.texcoord.xy, _MainTex);

		        o.viewDir = ObjSpaceViewDir ( v.vertex );
          
		        // Convert vertex position to world space
		        o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

		       	o.pos = UnityObjectToClipPos(v.vertex);
		       	o.grabUV = ComputeGrabScreenPos(o.pos);
                return o;
            }
           
            fixed4 frag (v2f i) : SV_Target
            {

            	// FRESNEL - - - - - - - - - - - -
		      
//		       	half rim = saturate(dot (normalize(i.viewDir), i.normal));
//		       	rim  = clamp(pow(rim,_Fresnel), 0, 1);

		    	// REFRACTION - - - - - - - - - - - -
				// normal mapping
//				half2 bump = i.normal.xy;// UnpackNormal(tex2D(_RefractionMap, N.xy)).rg;

				// calculate the offset for the refraction
//				float2 offset = bump * _GrabTexture_TexelSize.xy * _RefractionStrength * _RefractionStrength;

				// calculate uvs with the new offset
//				half4 uv2 = half4(i.grabUV.x + offset.x, i.grabUV.y + offset.y, i.grabUV.z, i.grabUV.w);

		    	//half fresnel = pow(1.0 - cosTheta, _Fresnel);


		    	half4 uv = i.texcoord;
                fixed4 grabCol = tex2Dproj(_GrabTexture, i.grabUV); //UNITY_PROJ_COORD(uv2));
                fixed4 texCol = tex2Dproj(_MainTex, uv); //UNITY_PROJ_COORD(uv2));

                float dist = distance( _TwirlCenter, uv);
                float2 tc = uv;
//                tc -= _TwirlCenter;
				if (dist < _TwirlRadius) 
				  {
				    float percent = (_TwirlRadius - dist) / _TwirlRadius;
				    float theta = percent * percent * _TwirlAngle * 8.0;
				    float s = sin(theta);
				    float c = cos(theta);
				    tc = float2(dot(tc, float2(c, -s)), dot(tc, float2(s, c)));
				  }
//			  	tc += _TwirlCenter;
			  	fixed4 twirlCol = tex2Dproj(_GrabTexture, i.grabUV + float4(tc, 0,1));


			  	float delta  = distance (_TwirlCenter.xy , uv.xy);
			  	float4 black = float4(0,0,0,0);
			  	if ( delta < _BlackHoleRadius)
			  	{
			  		black.a = smoothstep (1.0, 0 , delta/_BlackHoleRadius);

			  	}


//				half2 vn;
//				vn.x = dot(normalize( i.TtoV0), float3(0,0,1));
//				vn.y = dot(normalize( i.TtoV1), float3(0,0,1));
				
//				float4 matCap = tex2D(_MatCapMap, vn*0.5 + 0.5);

				// MIXING - - - - -  - - - 
				fixed4 col = twirlCol;// grabCol; // lerp(grabCol, matCap,1-rim);
				col = lerp( col, black, black.a);

//				col.rgb += thinFilm;
                //col.a = 1.0;

                //col.a = 1-rim;
               	//col.rgb = rim;
                return col;
            }
            ENDCG
        }
    }
}