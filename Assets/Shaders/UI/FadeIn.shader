// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:1,cusa:True,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:True,tesm:0,olmd:1,culm:2,bsrc:0,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:True,atwp:True,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:1873,x:34419,y:32725,varname:node_1873,prsc:2|emission-1086-OUT,alpha-8900-OUT;n:type:ShaderForge.SFN_Tex2d,id:4805,x:32430,y:32713,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:True,tagnsco:False,tagnrm:False,tex:6320ab525e2d2467292133c3748436d3,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1086,x:34199,y:32855,cmnt:RGB,varname:node_1086,prsc:2|A-4805-RGB,B-8900-OUT;n:type:ShaderForge.SFN_Color,id:5983,x:32005,y:32906,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_VertexColor,id:5376,x:32024,y:33070,varname:node_5376,prsc:2;n:type:ShaderForge.SFN_Smoothstep,id:9311,x:33633,y:33041,varname:node_9311,prsc:2|A-8730-OUT,B-4664-OUT,V-4268-OUT;n:type:ShaderForge.SFN_TexCoord,id:7761,x:32045,y:33244,varname:node_7761,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Slider,id:7711,x:32240,y:33074,ptovrint:False,ptlb:fadeIn,ptin:_fadeIn,varname:node_7711,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:8185,x:32240,y:32939,ptovrint:False,ptlb:fade_Size,ptin:_fade_Size,varname:node_8185,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.09150057,max:1;n:type:ShaderForge.SFN_Add,id:8730,x:33376,y:32964,varname:node_8730,prsc:2|A-7280-OUT,B-8185-OUT;n:type:ShaderForge.SFN_Subtract,id:4664,x:33376,y:33116,varname:node_4664,prsc:2|A-7280-OUT,B-8185-OUT;n:type:ShaderForge.SFN_Multiply,id:8900,x:34002,y:32963,varname:node_8900,prsc:2|A-4805-A,B-5536-OUT;n:type:ShaderForge.SFN_Slider,id:2576,x:32224,y:33198,ptovrint:False,ptlb:fadeOut,ptin:_fadeOut,varname:node_2576,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Subtract,id:8070,x:33376,y:33639,varname:node_8070,prsc:2|A-9149-OUT,B-8185-OUT;n:type:ShaderForge.SFN_Smoothstep,id:5392,x:33635,y:33507,varname:node_5392,prsc:2|A-8070-OUT,B-1204-OUT,V-7761-U;n:type:ShaderForge.SFN_Multiply,id:5536,x:33828,y:33037,varname:node_5536,prsc:2|A-9311-OUT,B-5392-OUT;n:type:ShaderForge.SFN_Multiply,id:7280,x:33155,y:32891,varname:node_7280,prsc:2|A-7711-OUT,B-4876-OUT;n:type:ShaderForge.SFN_Multiply,id:3872,x:32770,y:32786,varname:node_3872,prsc:2|A-8185-OUT,B-2039-OUT;n:type:ShaderForge.SFN_Vector1,id:2039,x:32588,y:32786,varname:node_2039,prsc:2,v1:2;n:type:ShaderForge.SFN_Add,id:4876,x:32968,y:32866,varname:node_4876,prsc:2|A-3872-OUT,B-7711-OUT;n:type:ShaderForge.SFN_Add,id:4268,x:33376,y:33264,varname:node_4268,prsc:2|A-7761-U,B-8185-OUT;n:type:ShaderForge.SFN_Add,id:8749,x:33016,y:33330,varname:node_8749,prsc:2|A-3872-OUT,B-2576-OUT;n:type:ShaderForge.SFN_Multiply,id:9149,x:33171,y:33443,varname:node_9149,prsc:2|A-8749-OUT,B-2576-OUT;n:type:ShaderForge.SFN_Add,id:1204,x:33376,y:33404,varname:node_1204,prsc:2|A-9149-OUT,B-8185-OUT;proporder:4805-5983-7711-8185-2576;pass:END;sub:END;*/

Shader "AERO/FadeIn" {
    Properties {
        [PerRendererData]_MainTex ("MainTex", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        _fadeIn ("fadeIn", Range(0, 1)) = 0
        _fade_Size ("fade_Size", Range(0, 1)) = 0.09150057
        _fadeOut ("fadeOut", Range(0, 1)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        _Stencil ("Stencil ID", Float) = 0
        _StencilReadMask ("Stencil Read Mask", Float) = 255
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilComp ("Stencil Comparison", Float) = 8
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilOpFail ("Stencil Fail Operation", Float) = 0
        _StencilOpZFail ("Stencil Z-Fail Operation", Float) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            Stencil {
                Ref [_Stencil]
                ReadMask [_StencilReadMask]
                WriteMask [_StencilWriteMask]
                Comp [_StencilComp]
                Pass [_StencilOp]
                Fail [_StencilOpFail]
                ZFail [_StencilOpZFail]
            }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _fadeIn;
            uniform float _fade_Size;
            uniform float _fadeOut;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_3872 = (_fade_Size*2.0);
                float node_7280 = (_fadeIn*(node_3872+_fadeIn));
                float node_9149 = ((node_3872+_fadeOut)*_fadeOut);
                float node_8900 = (_MainTex_var.a*(smoothstep( (node_7280+_fade_Size), (node_7280-_fade_Size), (i.uv0.r+_fade_Size) )*smoothstep( (node_9149-_fade_Size), (node_9149+_fade_Size), i.uv0.r )));
                float3 node_1086 = (_MainTex_var.rgb*node_8900); // RGB
                float3 emissive = node_1086;
                float3 finalColor = emissive;
                return fixed4(finalColor,node_8900);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos( v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
