﻿Shader "NEEEU/SimpleChromaKey"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_thresh ("Threshold", Range (0,1)) = 0.8
	    _slope ("Slope", Range (0,1)) = .1
	    _keyingColor ("Keying Color", Color) = (0,1,0,1)
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float3 _keyingColor;
			float _thresh; //0.8
			float _slope; //0.2

			fixed4 frag (v2f i) : SV_Target
			{
				//fixed4 col = tex2D(_MainTex, i.uv);
			
				float3 col = tex2D(_MainTex,i.uv).rgb;
				float3 d = abs(length(abs(_keyingColor.rgb - col.rgb)));
				float edge0 = _thresh*(1 - _slope);
				float alpha = smoothstep(edge0,_thresh,d);
				return fixed4(alpha, 0, 0,1);

			}
			ENDCG
		}
	}
}
