﻿Shader "NEEEU/ARCameraShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}

    	_textureY ("TextureY", 2D) = "white" {}
        _textureCbCr ("TextureCbCr", 2D) = "black" {}
        _texCoordScale ("Texture Coordinate Scale", float) = 1.0
        _isPortrait ("Device Orientation", Int) = 0
        _thresh ("Threshold", Range (0,1)) = 0.8
	    _slope ("Slope", Range (0,1)) = .1
	    _keyingColor ("Keying Color", Color) = (0,1,0,1)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
            ZWrite Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

            uniform float _texCoordScale;
            uniform int _isPortrait;
            float4x4 _TextureRotation;

			struct Vertex
			{
				float4 position : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct TexCoordInOut
			{
				float4 position : SV_POSITION;
				float2 texcoord : TEXCOORD0;
			};

			TexCoordInOut vert (Vertex vertex)
			{
				TexCoordInOut o;
				o.position = UnityObjectToClipPos(vertex.position);
				o.texcoord = vertex.texcoord;

				if (_isPortrait == 1)
				{
					o.texcoord = float2(vertex.texcoord.x, -(vertex.texcoord.y - 0.5f) * _texCoordScale + 0.5f);
				}
				else
				{
					o.texcoord = float2((vertex.texcoord.x - 0.5f) * _texCoordScale + 0.5f, -vertex.texcoord.y);
				}
				o.texcoord = mul(_TextureRotation, float4(o.texcoord,0,1)).xy;
	           
				return o;
			}
			
            // samplers
            sampler2D _MainTex;

            sampler2D _textureY;
            sampler2D _textureCbCr;

            float3 _keyingColor;
			float _thresh; //0.8
			float _slope; //0.2

			fixed4 frag (TexCoordInOut i) : SV_Target
			{
				// sample the texture
                float2 texcoord = i.texcoord;
                float y = tex2D(_textureY, texcoord).r;
                float4 ycbcr = float4(y, tex2D(_textureCbCr, texcoord).rg, 1.0);

				const float4x4 ycbcrToRGBTransform = float4x4(
						float4(1.0, +0.0000, +1.4020, -0.7010),
						float4(1.0, -0.3441, -0.7141, +0.5291),
						float4(1.0, +1.7720, +0.0000, -0.8860),
						float4(0.0, +0.0000, +0.0000, +1.0000)
					);
			
				fixed4 col = mul(ycbcrToRGBTransform, ycbcr);
				fixed4 texCol = tex2D(_MainTex,i.texcoord);
				//fixed4 col;
				float3 d = abs(length(abs(_keyingColor.rgb - col.rgb)));
				float edge0 = _thresh*(1 - _slope);
				float alpha = smoothstep(edge0,_thresh,d);
				//col.a = alpha;

				//col.rgb = lerp( col.rgb, texCol.rgb, alpha);
				return col;
			}
			ENDCG
		}
	}
}
